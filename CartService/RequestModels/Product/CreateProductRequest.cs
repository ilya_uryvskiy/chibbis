// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.RequestModels.Product
{
    using System.ComponentModel.DataAnnotations;
    using Chibbis.CartService.CustomAttributes;

    public class CreateProductRequest
    {
        [Required]
        [MinLength(3)]
        [MaxLength(64)]
        public string Name { get; set; }

        // [MinValue(0)]
        public decimal Cost { get; set; }

        public bool ForBonusPoints { get; set; }
    }
}
