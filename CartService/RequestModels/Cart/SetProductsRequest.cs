// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.RequestModels.Cart
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class SetProductsRequest
    {
        [MinLength(1)]
        public IEnumerable<SetProductsRequestProduct> Products { get; set; }
    }
}
