// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.RequestModels.Cart
{
    using System.ComponentModel.DataAnnotations;

    public class AddWebHookRequest
    {
        [Required]
        [MaxLength(1024)]
        public string CallbackUrl { get; set; }

        [Required]
        [MaxLength(512)]
        public string CallbackSecret { get; set; }
    }
}
