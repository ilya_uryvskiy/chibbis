// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.RequestModels.Cart
{
    using Chibbis.CartService.CustomAttributes;

    public class SetProductsRequestProduct
    {
        public int ProductId { get; set; }

        [MinValue(0)]
        public int Amount { get; set; }
    }
}
