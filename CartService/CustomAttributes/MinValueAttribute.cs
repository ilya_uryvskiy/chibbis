// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.CustomAttributes
{
    using System.ComponentModel.DataAnnotations;

    public class MinValueAttribute : ValidationAttribute
    {
        private readonly int minValue;

        public MinValueAttribute(int minValue)
            : base($"The value cannot be less than {minValue}")
        {
            this.minValue = minValue;
        }

        public override bool IsValid(object value)
        {
            return (int)value >= this.minValue;
        }
    }
}
