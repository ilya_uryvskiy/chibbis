// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.ResponseModels.Cart
{
    using System.Collections.Generic;

    public class GetWebhooksResponse : BaseResponse
    {
        public IEnumerable<GetWebhooksResponseWebhook> Webhooks { get; set; }
    }
}
