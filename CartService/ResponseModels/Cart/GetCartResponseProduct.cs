// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.ResponseModels.Cart
{
    public class GetCartResponseProduct
    {
        public int ProductId { get; set; }

        public int Amount { get; set; }
    }
}
