// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.ResponseModels.Cart
{
    public class GetWebhooksResponseWebhook
    {
        public int CartWebhookId { get; set; }

        public int CartId { get; set; }

        public string CallbackUrl { get; set; }
    }
}
