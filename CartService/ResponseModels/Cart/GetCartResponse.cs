// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.ResponseModels.Cart
{
    using System.Collections.Generic;

    public class GetCartResponse : BaseResponse
    {
        public int Id { get; set; }

        public IEnumerable<GetCartResponseProduct> Products { get; set; }
    }
}
