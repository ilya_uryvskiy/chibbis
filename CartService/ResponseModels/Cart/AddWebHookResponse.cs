// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.ResponseModels.Cart
{
    public class AddWebHookResponse
    {
        public int CartWebhookId { get; set; }

        public int CartId { get; set; }

        public string CallbackUrl { get; set; }
    }
}
