// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.ResponseModels.Cart
{
    public class CreateCartResponse : BaseResponse
    {
        public int CartId { get; set; }
    }
}
