// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.ResponseModels
{
    public class BaseResponse
    {
        public BaseResponse()
            : this(null)
        {
        }

        public BaseResponse(string message)
        {
            this.Message = message;
        }

        public string Message { get; set; }
    }
}
