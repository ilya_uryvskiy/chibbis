// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.ResponseModels.Product
{
    public class ProductResponseModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Cost { get; set; }

        public bool ForBonusPoints { get; set; }
    }
}
