// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.ResponseModels.Product
{
    using System.Collections.Generic;

    public class GetAllResponse : BaseResponse
    {
        public IEnumerable<ProductResponseModel> Products { get; set; }
    }
}
