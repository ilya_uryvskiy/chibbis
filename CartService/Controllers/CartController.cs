// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.Services;
    using Chibbis.CartService.RequestModels.Cart;
    using Chibbis.CartService.ResponseModels;
    using Chibbis.CartService.ResponseModels.Cart;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("[controller]")]
    public class CartController : ControllerBase
    {
        private readonly ICartService cartService;
        private readonly IProductService productService;

        public CartController(ICartService cartService, IProductService productService)
        {
            this.cartService = cartService ?? throw new ArgumentNullException(nameof(cartService));
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        [HttpGet("{cartId:int}")]
        public async Task<IActionResult> GetCart(int cartId)
        {
            var (cart, cartProducts) = await this.cartService.GetCartAndProductsAsync(cartId);

            if (cart == null)
            {
                return this.NotFound(new BaseResponse($"There's no cart with id {cartId}"));
            }

            var response = new GetCartResponse
            {
                Id = cart.Id,
                Products = cartProducts.Select(p => new GetCartResponseProduct
                {
                    ProductId = p.ProductId,
                    Amount = p.Amount
                })
            };
            return this.Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCart()
        {
            var cart = await this.cartService.NewCartAsync();
            var response = new CreateCartResponse
            {
                CartId = cart.Id
            };
            return this.Ok(response);
        }

        [HttpPost("{cartId:int}/product")]
        public async Task<IActionResult> SetProducts(int cartId, SetProductsRequest request)
        {
            var cartExists = await this.cartService.CheckIfCartExistsAsync(cartId);
            if (!cartExists)
            {
                return this.BadRequest(new BaseResponse($"There's no cart with id {cartId}"));
            }

            foreach (var product in request.Products)
            {
                if (!await this.productService.CheckIfProductExistsAsync(product.ProductId))
                {
                    return this.BadRequest(new BaseResponse($"There's no product with id {product.ProductId}"));
                }
            }

            foreach (var product in request.Products)
            {
                await this.cartService.SetProductAsync(cartId, product.ProductId, product.Amount);
            }

            var response = await this.GetCart(cartId);
            return response;
        }

        [HttpGet("{cartId:int}/webhook")]
        public async Task<IActionResult> GetWebhooks(int cartId)
        {
            var cartExists = await this.cartService.CheckIfCartExistsAsync(cartId);
            if (!cartExists)
            {
                return this.BadRequest(new BaseResponse($"There's no cart with id {cartId}"));
            }

            var webhooks = await this.cartService.GetCartWebhooksAsync(cartId);
            var response = new GetWebhooksResponse
            {
                Webhooks = webhooks.Select(one => new GetWebhooksResponseWebhook
                {
                    CartWebhookId = one.Id,
                    CartId = one.Id,
                    CallbackUrl = one.CallbackUrl
                })
            };

            return this.Ok(response);
        }

        [HttpPost("{cartId:int}/webhook")]
        public async Task<IActionResult> AddWebHook(int cartId, AddWebHookRequest request)
        {
            var cartExists = await this.cartService.CheckIfCartExistsAsync(cartId);
            if (!cartExists)
            {
                return this.BadRequest(new BaseResponse($"There's no cart with id {cartId}"));
            }

            var webhook = await this.cartService.AddCartWebhookAsync(cartId, request.CallbackUrl, request.CallbackSecret);
            var response = new AddWebHookResponse
            {
                CartWebhookId = webhook.Id,
                CartId = webhook.CartId,
                CallbackUrl = webhook.CallbackUrl
            };
            return this.Ok(response);
        }

        [HttpDelete("{cartId:int}/webhook/{cartWebhookId:int}")]
        public async Task<IActionResult> DeleteWebhook(int cartId, int cartWebhookId)
        {
            var cartExists = await this.cartService.CheckIfCartExistsAsync(cartId);
            if (!cartExists)
            {
                return this.BadRequest(new BaseResponse($"There's no cart with id {cartId}"));
            }

            await this.cartService.DeleteCartWebhookAsync(cartId, cartWebhookId);
            return this.Ok();
        }
    }
}
