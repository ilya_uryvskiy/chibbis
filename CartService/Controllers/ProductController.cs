// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;
    using Chibbis.CartService.Core.Services;
    using Chibbis.CartService.RequestModels.Product;
    using Chibbis.CartService.ResponseModels.Product;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var products = await this.productService.GetAllAsync();
            var response = new GetAllResponse
            {
                Products = products.Select(this.MapProductToProductResponseModel)
            };

            return this.Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProduct(CreateProductRequest request)
        {
            var newProduct = await this.productService.AddProductAsync(request.Name, request.Cost, request.ForBonusPoints);
            var productResponseModel = this.MapProductToProductResponseModel(newProduct);
            return this.Ok(productResponseModel);
        }

        private ProductResponseModel MapProductToProductResponseModel(Product product)
        {
            return new ProductResponseModel
            {
                Id = product.Id,
                Name = product.Name,
                Cost = product.Cost,
                ForBonusPoints = product.ForBonusPoints
            };
        }
    }
}
