// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.DbMigrations.Migrations
{
    using FluentMigrator;

    [Migration(202002251301)]
    public class PopulateProductsTable : Migration
    {
        public override void Up()
        {
            this.Insert.IntoTable("Products")
                .Row(new { Name = "Creamy ham, leek & mushroom spaghetti", Cost = 14.32M, ForBonusPoints = false })
                .Row(new { Name = "Bean & sausage hotpot", Cost = 7.44M, ForBonusPoints = true })
                .Row(new { Name = "Chicken & bean enchiladas", Cost = 11.0M, ForBonusPoints = false })
                .Row(new { Name = "Swedish meatball burgers", Cost = 21.1M, ForBonusPoints = true })
                .Row(new { Name = "Simple stir-fry", Cost = 19.65M, ForBonusPoints = false })
                .Row(new { Name = "Puttanesca meatball bake", Cost = 5.9M, ForBonusPoints = true })
                .Row(new { Name = "Slow cooker chilli", Cost = 3.89M, ForBonusPoints = false })
                .Row(new { Name = "Classic meatloaf with tomato sauce", Cost = 2.2M, ForBonusPoints = false })
                .Row(new { Name = "Turkey burgers with sweet potato chips", Cost = 34.5M, ForBonusPoints = false });
        }

        public override void Down()
        {
        }
    }
}
