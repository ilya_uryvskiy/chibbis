// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.DbMigrations.Migrations
{
    using System.Data;
    using FluentMigrator;

    [Migration(202002241853)]
    public class CreateCartProductsTable : Migration
    {
        public override void Up()
        {
            this.Create.Table("CartProducts")
                .WithColumn("Id").AsInt32().NotNullable().Identity().PrimaryKey()
                .WithColumn("CartId").AsInt32().NotNullable()
                .ForeignKey("Carts", "Id").OnDelete(Rule.Cascade)
                .Indexed()
                .WithColumn("ProductId").AsInt32().NotNullable()
                .ForeignKey("Products", "Id").OnDelete(Rule.Cascade)
                .WithColumn("Amount").AsInt32().NotNullable();
        }

        public override void Down()
        {
            this.Delete.Table("CartProducts");
        }
    }
}
