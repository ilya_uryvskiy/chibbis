// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.DbMigrations.Migrations
{
    using FluentMigrator;

    [Migration(202002241643)]
    public class CreateProductsTable : Migration
    {
        public override void Up()
        {
            this.Create.Table("Products")
                .WithColumn("Id").AsInt32().NotNullable().Identity().PrimaryKey()
                .WithColumn("Name").AsString(64).NotNullable()
                .WithColumn("Cost").AsDecimal().NotNullable()
                .WithColumn("ForBonusPoints").AsBoolean().NotNullable();
        }

        public override void Down()
        {
            this.Delete.Table("Products");
        }
    }
}
