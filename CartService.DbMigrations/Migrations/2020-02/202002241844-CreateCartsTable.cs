// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.DbMigrations.Migrations
{
    using FluentMigrator;

    [Migration(202002241844)]
    public class CreateCartsTable : Migration
    {
        public override void Up()
        {
            this.Create.Table("Carts")
                .WithColumn("Id").AsInt32().NotNullable().Identity().PrimaryKey()
                .WithColumn("LastAccessDt").AsDateTime().NotNullable().Indexed();
        }

        public override void Down()
        {
            this.Delete.Table("Carts");
        }
    }
}
