// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.DbMigrations.Migrations
{
    using System.Data;
    using FluentMigrator;

    [Migration(202002241906)]
    public class CreateCartWebhooks : Migration
    {
        public override void Up()
        {
            this.Create.Table("CartWebhooks")
                .WithColumn("Id").AsInt32().NotNullable().Identity().PrimaryKey()
                .WithColumn("CartId").AsInt32().NotNullable()
                .ForeignKey("Carts", "Id").OnDelete(Rule.Cascade)
                .Indexed()
                .WithColumn("CallbackUrl").AsString(1024).NotNullable()
                .WithColumn("CallbackSecret").AsString(512).NotNullable();
        }

        public override void Down()
        {
            this.Delete.Table("CartWebhooks");
        }
    }
}
