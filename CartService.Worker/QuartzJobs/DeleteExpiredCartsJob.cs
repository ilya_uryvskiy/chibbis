// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Worker.QuartzJobs
{
    using System;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.Services;
    using Quartz;

    [DisallowConcurrentExecution]
    internal class DeleteExpiredCartsJob : IJob
    {
        private readonly ICartService cartService;

        public DeleteExpiredCartsJob(ICartService cartService)
        {
            this.cartService = cartService ?? throw new ArgumentNullException(nameof(cartService));
        }

        public Task Execute(IJobExecutionContext context)
        {
            return this.cartService.DeleteExpiredCartsAsync();
        }
    }
}
