// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Worker.QuartzJobs
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.Services;
    using Microsoft.Extensions.Configuration;
    using Quartz;

    [DisallowConcurrentExecution]
    internal class DailyReportJob : IJob
    {
        private readonly ICartService cartService;
        private readonly string dailyReportPath;

        public DailyReportJob(ICartService cartService, IConfiguration configuration)
        {
            this.cartService = cartService ?? throw new ArgumentNullException(nameof(cartService));
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            this.dailyReportPath = configuration["DailyReportPath"];
            if (string.IsNullOrEmpty(this.dailyReportPath))
            {
                throw new ArgumentException("DailyReportPath is null or empty", nameof(configuration));
            }
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var dailyReport = await this.cartService.BuildCartReportAsync();
            var reportLine = string.Empty;
            reportLine += $"{dailyReport.CartCount};";
            reportLine += $"{dailyReport.CartWithProductForBonusPointsCount};";
            reportLine += $"{dailyReport.CartExpiresInTenDays};";
            reportLine += $"{dailyReport.CartExpiresInTwentyDays};";
            reportLine += $"{dailyReport.CartExpiresInThirtyDays};";
            reportLine += $"{dailyReport.AverageInvoice};";
            await File.AppendAllLinesAsync(this.dailyReportPath, new[] { reportLine });
        }
    }
}
