// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Worker
{
    using System;
    using Quartz;
    using Quartz.Spi;

    internal class JobFactory : IJobFactory
    {
        private readonly IServiceProvider serviceProvider;

        public JobFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            var jobType = bundle.JobDetail.JobType;
            var job = new AsyncScopedLifestyleJobDecorator(this.serviceProvider, jobType);
            return job;
        }

        public void ReturnJob(IJob job)
        {
            (job as IDisposable)?.Dispose();
        }
    }
}
