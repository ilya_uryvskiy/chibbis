// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Worker
{
    using System;
    using System.Threading.Tasks;
    using Chibbis.CartService.Worker.QuartzJobs;
    using Quartz;

    internal class SchedulerManager
    {
        private readonly IScheduler scheduler;

        public SchedulerManager(IScheduler scheduler)
        {
            this.scheduler = scheduler ?? throw new ArgumentNullException(nameof(scheduler));
        }

        public async Task ScheduleTasks()
        {
            // Schedule DeleteExpiredCartsJob to run every hour
            var deleteExpiredCartsJobDetail = JobBuilder.Create<DeleteExpiredCartsJob>().Build();
            var deleteExpiredCartsJobTrigger = TriggerBuilder.Create()
                .WithCronSchedule("0 0 * * * ?")
                .StartNow().Build();
            await this.scheduler.ScheduleJob(deleteExpiredCartsJobDetail, deleteExpiredCartsJobTrigger);

            // Schedule DailyReportJob to run every day at 8 AM
            var dailyReportJobDetail = JobBuilder.Create<DailyReportJob>().Build();
            var dailyReportJobTrigger = TriggerBuilder.Create()
                .WithCronSchedule("0 0 8 * * ?")
                .StartNow().Build();
            await this.scheduler.ScheduleJob(dailyReportJobDetail, dailyReportJobTrigger);

            await this.scheduler.Start();
        }
    }
}
