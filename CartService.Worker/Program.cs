﻿// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Worker
{
    using System;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    internal class Program
    {
        private static async Task Main(string[] args)
        {
            var configuration = BuildConfiguration();
            var serviceProvider = BuildServiceProvider(configuration);
            var schedulerManager = serviceProvider.GetService<SchedulerManager>();
            await schedulerManager.ScheduleTasks();
            Console.ReadLine();
        }

        private static IConfiguration BuildConfiguration()
        {
            var configBuilder = new ConfigurationBuilder();
            configBuilder.AddJsonFile("appsettings.json", false, false);
            return configBuilder.Build();
        }

        private static IServiceProvider BuildServiceProvider(IConfiguration configuration)
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IConfiguration>(configuration);

            serviceCollection.AddQuartz();
            serviceCollection.AddCore(configuration);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            return serviceProvider;
        }
    }
}
