// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Worker
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.DependencyInjection;
    using Quartz;

    internal class AsyncScopedLifestyleJobDecorator : IJob
    {
        private readonly IServiceProvider serviceProvider;
        private readonly Type jobType;

        public AsyncScopedLifestyleJobDecorator(IServiceProvider serviceProvider, Type jobType)
        {
            this.serviceProvider = serviceProvider;
            this.jobType = jobType;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            using (var scope = this.serviceProvider.CreateScope())
            {
                if (scope.ServiceProvider.GetService(this.jobType) is IJob job)
                {
                    await job.Execute(context);
                }
            }
        }
    }
}
