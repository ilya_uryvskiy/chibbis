// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Worker
{
    using System.Collections.Specialized;
    using Chibbis.CartService.Worker.QuartzJobs;
    using Microsoft.Extensions.DependencyInjection;
    using Quartz;
    using Quartz.Impl;
    using Quartz.Spi;

    internal static class QuartzInstaller
    {
        public static IServiceCollection AddQuartz(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IJobFactory, JobFactory>();
            serviceCollection.AddSingleton<IScheduler>(serviceProvider =>
            {
                var props = new NameValueCollection
                {
                    { "quartz.serializer.type", "binary" }
                };
                var factory = new StdSchedulerFactory(props);
                var scheduler = factory.GetScheduler().Result;
                scheduler.JobFactory = serviceProvider.GetService<IJobFactory>();
                return scheduler;
            });

            serviceCollection.AddTransient<DeleteExpiredCartsJob>();
            serviceCollection.AddTransient<SchedulerManager>();
            serviceCollection.AddTransient<DailyReportJob>();
            return serviceCollection;
        }
    }
}
