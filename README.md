# Cart Service
## Описание
Солюшин состоит из 4 проектов:

* CartService - проект веб апи
* CartService.Worker - проект с бэкграунд джобами
* CartService.DbMigrations - миграции базы данных
* CartService.Core - содержит бизнес логику и код доступа к БД 
 
## CartService
Веб апи для сервиса корзины. Позволяет:

* Добавлять и получать список продуктов
* Создавать, получать и изменять содержимое корзины
* Устанавливать, получать список и удалять веб хуки для корзины

### Примеры запросов
#### Получить список продуктов:

```
curl --location --request GET 'http://localhost:5000/product'
```

#### Добавить продукт:

```
curl --location --request POST 'http://localhost:5000/product' \
--header 'Content-Type: application/json' \
--data-raw '{
	"Name":"Kebab",
	"Cost": 2.0,
	"ForBonusPoints": false
}'
```

#### Создать корзину:

```
curl --location --request POST 'http://localhost:5000/cart' --header 'Content-Type: application/json' --data-raw '{}'
```
Ответ будет содержать Id вновь созданной корзины

```
{
    "cartId": 4,
    "message": null
}
```

#### Получить корзину:

```
curl --location --request GET 'http://localhost:5000/cart/{{cartId}}'
```
Где {{cartId}} - Id корзины

Пример ответа
```
{
    "id": 2,
    "products": [
        {
            "productId": 1,
            "amount": 2
        },
        {
            "productId": 3,
            "amount": 2
        }
    ],
    "message": null
}
```
Ответ содержит в себе массив входящих в корзину продуктов и их количество

#### Изменить содержимое корзины:

```
curl --location --request POST 'http://localhost:5000/cart/2/product' \
--header 'Content-Type: application/json' \
--data-raw '{
	"Products":
	[
		{ "ProductId": 1, "Amount": 2 },
		{ "ProductId": 3, "Amount": 1 }
	]
}'
```
Для изменениия содержимого корзины необходимо отправить
запрос который будет содержать массов тех продуктов которые необходимо изменить.
Если изначально продукта не было в корзине, то он туда добавиться. Если продукт был,
то измениться количество продукта в корзине. Если в качетве Amount передать 0, то
продукт будет удалён из корзины.

В ответ придёт новое состояние корзины.

#### Установить вэбхук для корзины:

```
curl --location --request POST 'http://localhost:5000/cart/{{cartId}}/webhook' \
--header 'Content-Type: application/json' \
--data-raw '{
	"CallbackUrl": "https://foo.bar",
	"CallbackSecret": "0xDEADBEEF"
}'
```

Где {{cartId}} - Id корзины для которой устанавливаем вебхук,
CallbackUrl - адресс обратного вызова,
CallbackSecret - секретное значение которое будет пререданно в заголовке
X-CartAuth при обратном вызове. Обратный вызов будет POST запросом.
Тело запроса будет содержать json объект с Id удаляемой корзины.
 
```json
{ 
  "cartId": 314 
}

```

#### Получить список вебхуков для корзины:

```
curl --location --request GET 'http://localhost:5000/cart/{{cartId}}/webhook'
```

Где {{cartId}} - Id корзины

#### Удалить вебхук:

```
curl --location --request DELETE 'http://localhost:5000/cart/{{cartId}}/webhook/{{webhookId}}'
```
Где {{cartId}} - Id корзины, а {{webhookId}} - Id вебхука

## CartService.Worker
Сервис для бэкграунд джобов. Использует Quartz.NET для шедулинга джобов.

* DailyReportJob - джоба собирает ежедневный отчёт. Запускается каждый день в 8 утра.
* DeleteExpiredCartsJob - джоба для удаления заэкспрайреных корзин.

## CartService.DbMigrations
Консольное приложение содержащее в себе миграции для БД.
Для миграций используется фрэймворк FluentMigrator.
Для корректной работы база данных на которую накатываем
миграции должна существовать.

## CartService.Core
Проект содержит бизнес логику и логику доступа к БД.
В качестве ORM используется Dapper.

## Как собрать и запустить
Чтобы собрать солюшен в корне проекта надо выполнить

```
dotnet build
```

Проекты CartService, CartService.Worker и CartService.DbMigrations
используют следующую строку подключения

```
Server=localhost;Database=chibbis;User Id=sa;Password=P@ssw0rd;
```

Можно либо использовать docker-compose.yml файл в корне проекта и затем создать
базу chibbis в поднятом MS SQL сервере либо поменять строки подключения.

Прежде чем запускать проекты CartService и CartService.Worker необходимо
прогнать миграции из CartService.DbMigrations чтобы создать схему.

```
dotnet run --project  ./CartService.DbMigrations/CartService.DbMigrations.csproj 
```

Для запуска CartService и CartService.Worker можно использовать след команды

```
dotnet run --project  ./CartService/CartService.csproj

dotnet run --project  ./CartService.Worker/CartService.Worker.csproj
```
