// Copyright (c) Ilya Uryvskiy. All rights reserved.

using System.Diagnostics.CodeAnalysis;

[assembly:SuppressMessage("Microsoft.Naming", "SA0001:XmlCommentAnalysisDisabled", Justification = "Reviewed")]
[assembly:SuppressMessage("Microsoft.Naming", "SA1600:ElementsMustBeDocumented", Justification = "Reviewed")]
[assembly:SuppressMessage("Microsoft.Naming", "SA1413:UseTrailingCommasInMultiLineInitializers", Justification = "Reviewed")]
