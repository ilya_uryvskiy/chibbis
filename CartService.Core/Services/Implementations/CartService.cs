// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;
    using Chibbis.CartService.Core.DataAccessLayer.Repos;
    using Chibbis.CartService.Core.DateTime;
    using Chibbis.CartService.Core.ReportModels;
    using Microsoft.Extensions.Logging;
    using Polly;

    internal class CartService : ICartService
    {
        private readonly IDateTime dateTime;
        private readonly ICartRepo cartRepo;
        private readonly ICartProductRepo cartProductRepo;
        private readonly ICartWebhookRepo cartWebhookRepo;
        private readonly IReportRepo reportRepo;
        private readonly IHttpClientFactory httpClientFactory;
        private readonly ILogger<CartService> logger;

        public CartService(
            IDateTime dateTime,
            ICartRepo cartRepo,
            ICartProductRepo cartProductRepo,
            ICartWebhookRepo cartWebhookRepo,
            IReportRepo reportRepo,
            IHttpClientFactory httpClientFactory,
            ILogger<CartService> logger)
        {
            this.dateTime = dateTime ?? throw new ArgumentNullException(nameof(dateTime));
            this.cartRepo = cartRepo ?? throw new ArgumentNullException(nameof(cartRepo));
            this.cartProductRepo = cartProductRepo ?? throw new ArgumentNullException(nameof(cartProductRepo));
            this.cartWebhookRepo = cartWebhookRepo ?? throw new ArgumentNullException(nameof(cartWebhookRepo));
            this.reportRepo = reportRepo ?? throw new ArgumentNullException(nameof(reportRepo));
            this.httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<(Cart, IEnumerable<CartProduct>)> GetCartAndProductsAsync(int cartId)
        {
            var cart = await this.cartRepo.GetCartByIdAsync(cartId);
            if (cart == null)
            {
                return (null, Array.Empty<CartProduct>());
            }

            var cartProducts = await this.cartProductRepo.GetCartProductsAsync(cartId);

            return (cart, cartProducts);
        }

        public Task<Cart> NewCartAsync()
        {
            return this.cartRepo.AddCartAsync(this.dateTime.UtcNow);
        }

        public Task<bool> CheckIfCartExistsAsync(int cartId)
        {
            return this.cartRepo.CheckCartExistsAsync(cartId);
        }

        public async Task SetProductAsync(int cartId, int productId, int amount)
        {
            var cartProduct = await this.cartProductRepo.GetCartProductAsync(cartId, productId);
            if (amount == 0)
            {
                if (cartProduct == null)
                {
                    return;
                }

                await this.cartProductRepo.DeleteCartProductAsync(cartProduct.Id);
            }
            else
            {
                if (cartProduct == null)
                {
                    await this.cartProductRepo.AddCartProductAsync(cartId, productId, amount);
                }
                else
                {
                    await this.cartProductRepo.UpdateCartProductAmountAsync(cartProduct.Id, amount);
                }
            }

            await this.cartRepo.SetLastAccessDtAsync(cartId, this.dateTime.UtcNow);
        }

        public Task<IEnumerable<CartWebhook>> GetCartWebhooksAsync(int cartId)
        {
            return this.cartWebhookRepo.GetCartWebhooksAsync(cartId);
        }

        public Task<CartWebhook> AddCartWebhookAsync(int cartId, string callbackUrl, string callbackSecret)
        {
            return this.cartWebhookRepo.AddCartWebhookAsync(cartId, callbackUrl, callbackSecret);
        }

        public Task DeleteCartWebhookAsync(int cartId, int cartWebhookId)
        {
            return this.cartWebhookRepo.DeleteCartWebkookAsync(cartId, cartWebhookId);
        }

        public async Task DeleteExpiredCartsAsync()
        {
            var notBefore = this.dateTime.UtcNow.AddDays(-Constants.CartExpireDays);
            var cartsToDelete = await this.cartRepo.GetCartsWithLastAccessDtLessThenAsync(notBefore);

            foreach (var cart in cartsToDelete)
            {
                await this.DeleteCartAsync(cart.Id);
            }
        }

        public async Task<DailyReport> BuildCartReportAsync()
        {
            var report = new DailyReport
            {
                CartCount = await this.reportRepo.GetCartCountAsync(),
                CartWithProductForBonusPointsCount = await this.reportRepo.GetCartWithForBonusPointsProductCountAsync(),
                CartExpiresInTenDays = await this.reportRepo.GetExpiredCartCountInDaysAsync(10),
                CartExpiresInTwentyDays = await this.reportRepo.GetExpiredCartCountInDaysAsync(20),
                CartExpiresInThirtyDays = await this.reportRepo.GetExpiredCartCountInDaysAsync(30),
                AverageInvoice = await this.reportRepo.GetAverageInvoiceAsync()
            };
            return report;
        }

        private async Task DeleteCartAsync(int cartId)
        {
            var cartWebhooks = await this.cartWebhookRepo.GetCartWebhooksAsync(cartId);
            var notificationTasks = new List<Task>();
            foreach (var cartWebhook in cartWebhooks)
            {
                var notificationTask = this.NotifyCartDeletedAsync(cartId, cartWebhook);
                notificationTasks.Add(notificationTask);
            }

            await Task.WhenAll(notificationTasks);
            await this.cartRepo.DeleteCartAsync(cartId);
        }

        private async Task NotifyCartDeletedAsync(int cartId, CartWebhook cartWebhook)
        {
            try
            {
                var httpClient = this.httpClientFactory.CreateClient();
                httpClient.DefaultRequestHeaders.Add(Constants.CartWebhookAuthHeader, cartWebhook.CallbackSecret);
                var payload = new { cartId };
                var payloadStr = JsonSerializer.Serialize(payload);
                var httpContent = new StringContent(payloadStr, Encoding.UTF8, "application/json");

                var retryPolicy = Policy.Handle<Exception>().WaitAndRetryAsync(new[]
                {
                    TimeSpan.FromSeconds(10),
                    TimeSpan.FromSeconds(20),
                    TimeSpan.FromSeconds(30),
                });
                await retryPolicy.ExecuteAsync(async () =>
                {
                    var resp = await httpClient.PostAsync(cartWebhook.CallbackUrl, httpContent);
                    resp.EnsureSuccessStatusCode();
                });
            }
            catch (Exception e)
            {
                this.logger.LogError(e, "Error occured while calling cart delete webhook");
            }
        }
    }
}
