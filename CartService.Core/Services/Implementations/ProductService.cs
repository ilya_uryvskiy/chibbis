// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;
    using Chibbis.CartService.Core.DataAccessLayer.Repos;
    using Microsoft.Extensions.Logging;

    internal class ProductService : IProductService
    {
        private readonly IProductRepo productRepo;
        private readonly ILogger<ProductService> logger;

        public ProductService(IProductRepo productRepo, ILogger<ProductService> logger)
        {
            this.productRepo = productRepo ?? throw new ArgumentNullException(nameof(productRepo));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Task<IEnumerable<Product>> GetAllAsync()
        {
            return this.productRepo.GetAllAsync();
        }

        public Task<Product> AddProductAsync(string name, decimal cost, bool forBonusPoints)
        {
            this.logger.LogInformation($"Adding new product; Name: {name}; Cost: {cost}; ForBonusPoints: {forBonusPoints}");
            return this.productRepo.AddProductAsync(name, cost, forBonusPoints);
        }

        public async Task<bool> CheckIfProductExistsAsync(int id)
        {
            var product = await this.productRepo.GetProductByIdAsync(id);
            return product != null;
        }
    }
}
