// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;
    using Chibbis.CartService.Core.ReportModels;

    public interface ICartService
    {
        Task<(Cart, IEnumerable<CartProduct>)> GetCartAndProductsAsync(int cartId);

        Task<Cart> NewCartAsync();

        Task<bool> CheckIfCartExistsAsync(int cartId);

        Task SetProductAsync(int cart, int productId, int amount);

        Task<IEnumerable<CartWebhook>> GetCartWebhooksAsync(int cartId);

        Task<CartWebhook> AddCartWebhookAsync(int cartId, string callbackUrl, string callbackSecret);

        Task DeleteCartWebhookAsync(int cartId, int cartWebhookId);

        Task DeleteExpiredCartsAsync();

        Task<DailyReport> BuildCartReportAsync();
    }
}
