// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;

    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAllAsync();

        Task<Product> AddProductAsync(string name, decimal cost, bool forBonusPoints);

        Task<bool> CheckIfProductExistsAsync(int id);
    }
}
