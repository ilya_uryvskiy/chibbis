// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Models
{
    public class CartWebhook
    {
        public int Id { get; set; }

        public int CartId { get; set; }

        public string CallbackUrl { get; set; }

        public string CallbackSecret { get; set; }
    }
}
