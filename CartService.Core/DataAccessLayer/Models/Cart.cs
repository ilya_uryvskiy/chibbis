// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Models
{
    using System;

    public class Cart
    {
        public int Id { get; set; }

        public DateTime LastAccessDt { get; set; }
    }
}
