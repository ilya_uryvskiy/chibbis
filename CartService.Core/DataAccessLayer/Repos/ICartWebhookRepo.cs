// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;

    internal interface ICartWebhookRepo
    {
        Task<IEnumerable<CartWebhook>> GetCartWebhooksAsync(int cartId);

        Task<CartWebhook> AddCartWebhookAsync(int cartId, string callbackUrl, string callbackSecret);

        Task DeleteCartWebkookAsync(int cartId, int cartWebhookId);
    }
}
