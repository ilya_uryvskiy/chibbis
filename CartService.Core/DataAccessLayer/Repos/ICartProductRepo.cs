// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;

    internal interface ICartProductRepo
    {
        Task<IEnumerable<CartProduct>> GetCartProductsAsync(int cartId);

        Task<CartProduct> GetCartProductAsync(int cartId, int productId);

        Task DeleteCartProductAsync(int cartProductId);

        Task AddCartProductAsync(int cartId, int productId, int amount);

        Task UpdateCartProductAmountAsync(int cartProductId, int amount);
    }
}
