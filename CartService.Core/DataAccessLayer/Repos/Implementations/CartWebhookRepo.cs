// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos.Implementations
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;
    using Dapper;
    using Microsoft.Extensions.Configuration;

    internal class CartWebhookRepo : BaseRepo, ICartWebhookRepo
    {
        public CartWebhookRepo(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<IEnumerable<CartWebhook>> GetCartWebhooksAsync(int cartId)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "SELECT Id, CartId, CallbackUrl, CallbackSecret FROM CartWebhooks WHERE CartId=@cartId";
                var webhooks = await connection.QueryAsync<CartWebhook>(sql, new { cartId });
                return webhooks;
            }
        }

        public async Task<CartWebhook> AddCartWebhookAsync(int cartId, string callbackUrl, string callbackSecret)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "INSERT INTO CartWebhooks (CartId, CallbackUrl, CallbackSecret) OUTPUT INSERTED.Id VALUES (@cartId, @callbackUrl, @callbackSecret)";
                var id = await connection.QuerySingleAsync<int>(sql, new { cartId, callbackUrl, callbackSecret });

                var cartWebhook = new CartWebhook
                {
                    Id = id,
                    CartId = cartId,
                    CallbackUrl = callbackUrl,
                    CallbackSecret = callbackSecret
                };
                return cartWebhook;
            }
        }

        public async Task DeleteCartWebkookAsync(int cartId, int cartWebhookId)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "DELETE FROM CartWebhooks WHERE Id=@cartWebhookId AND CartId=@cartId";
                await connection.ExecuteAsync(sql, new { cartId, cartWebhookId });
            }
        }
    }
}
