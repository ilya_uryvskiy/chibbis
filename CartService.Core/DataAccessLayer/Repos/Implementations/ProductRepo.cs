// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos.Implementations
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;
    using Dapper;
    using Microsoft.Extensions.Configuration;

    internal class ProductRepo : BaseRepo, IProductRepo
    {
        public ProductRepo(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "SELECT Id, Name, Cost, ForBonusPoints FROM Products";
                var result = await connection.QueryAsync<Product>(sql);
                return result;
            }
        }

        public async Task<Product> GetProductByIdAsync(int id)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "SELECT Id, Name, Cost, ForBonusPoints FROM Products WHERE Id=@id";
                var product = await connection.QueryFirstOrDefaultAsync<Product>(sql, new { id });
                return product;
            }
        }

        public async Task<Product> AddProductAsync(string name, decimal cost, bool forBonusPoints)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "INSERT INTO Products (Name, Cost, ForBonusPoints) OUTPUT INSERTED.Id VALUES (@name, @cost, @forBonusPoints)";
                var id = await connection.QuerySingleAsync<int>(sql, new { name, cost, forBonusPoints });

                var product = new Product
                {
                    Id = id,
                    Name = name,
                    Cost = cost,
                    ForBonusPoints = forBonusPoints
                };
                return product;
            }
        }
    }
}
