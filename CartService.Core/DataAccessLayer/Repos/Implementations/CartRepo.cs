// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;
    using Dapper;
    using Microsoft.Extensions.Configuration;

    internal class CartRepo : BaseRepo, ICartRepo
    {
        public CartRepo(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<bool> CheckCartExistsAsync(int id)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "SELECT 1 FROM Carts WHERE Id=@id";
                var result = await connection.QueryFirstOrDefaultAsync<bool>(sql, new { id });
                return result;
            }
        }

        public async Task<Cart> GetCartByIdAsync(int id)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "SELECT Id, LastAccessDt FROM Carts WHERE Id=@id";
                var cart = await connection.QueryFirstOrDefaultAsync<Cart>(sql, new { id });
                return cart;
            }
        }

        public async Task<Cart> AddCartAsync(DateTime lastAccessDt)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "INSERT INTO Carts (LastAccessDt) OUTPUT INSERTED.Id VALUES (@lastAccessDt)";
                var id = await connection.QuerySingleAsync<int>(sql, new { lastAccessDt });

                var cart = new Cart
                {
                    Id = id,
                    LastAccessDt = lastAccessDt
                };
                return cart;
            }
        }

        public async Task SetLastAccessDtAsync(int id, DateTime lastAccessDt)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "UPDATE Carts SET LastAccessDt=@lastAccessDt WHERE Id=@id";
                await connection.ExecuteAsync(sql, new { id, lastAccessDt });
            }
        }

        public async Task<IEnumerable<Cart>> GetCartsWithLastAccessDtLessThenAsync(DateTime lastAccessDt)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "SELECT Id, LastAccessDt FROM Carts WHERE LastAccessDt<@lastAccessDt";
                var carts = await connection.QueryAsync<Cart>(sql, new { lastAccessDt });
                return carts;
            }
        }

        public async Task DeleteCartAsync(int id)
        {
            // Since all FKs have cascade on delete policy we can just delete a cart
            using (var connection = this.GetDbConnection())
            {
                var sql = "DELETE FROM Carts WHERE Id=@id";
                await connection.ExecuteAsync(sql, new { id });
            }
        }
    }
}
