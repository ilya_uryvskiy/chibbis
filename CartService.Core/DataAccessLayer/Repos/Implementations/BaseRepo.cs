// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos.Implementations
{
    using System;
    using System.Data;
    using Microsoft.Data.SqlClient;
    using Microsoft.Extensions.Configuration;

    public class BaseRepo
    {
        private readonly string connectionString;

        protected BaseRepo(IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            this.connectionString = configuration.GetConnectionString(Constants.DefaultConnectionStringName);

            if (string.IsNullOrEmpty(this.connectionString))
            {
                var errorMessage = $"Connection string with name '{Constants.DefaultConnectionStringName}' is null or empty";
                throw new ArgumentException(errorMessage, nameof(configuration));
            }
        }

        protected IDbConnection GetDbConnection()
        {
            var connection = new SqlConnection(this.connectionString);
            return connection;
        }
    }
}
