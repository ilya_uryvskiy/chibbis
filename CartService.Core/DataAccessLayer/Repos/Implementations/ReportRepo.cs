// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos.Implementations
{
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.Extensions.Configuration;

    internal class ReportRepo : BaseRepo, IReportRepo
    {
        public ReportRepo(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<int> GetCartCountAsync()
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "SELECT COUNT(1) FROM Carts";
                var count = await connection.ExecuteScalarAsync<int>(sql);
                return count;
            }
        }

        public async Task<int> GetCartWithForBonusPointsProductCountAsync()
        {
            using (var connection = this.GetDbConnection())
            {
                var sql =
@"SELECT COUNT(1) FROM (
SELECT DISTINCT cp.CartId FROM CartProducts as cp
JOIN Products as p on cp.ProductId = p.Id
WHERE p.ForBonusPoints = 1) as P";
                var count = await connection.ExecuteScalarAsync<int>(sql);
                return count;
            }
        }

        public async Task<int> GetExpiredCartCountInDaysAsync(int days)
        {
            using (var connection = this.GetDbConnection())
            {
                var shift = Constants.CartExpireDays - days;
                var sql = "SELECT COUNT(1) FROM Carts WHERE LastAccessDt < DATEADD(DD, -@shift, GETUTCDATE())";
                var count = await connection.ExecuteScalarAsync<int>(sql, new { shift });
                return count;
            }
        }

        public async Task<decimal> GetAverageInvoiceAsync()
        {
            using (var connection = this.GetDbConnection())
            {
                var sql =
@"SELECT AVG(CartInvoice) FROM (
SELECT SUM(p.Cost*cp.Amount) AS CartInvoice
FROM CartProducts as cp
INNER JOIN Products p on cp.ProductId = p.Id
GROUP BY cp.CartId) AS P";
                var averageInvoice = await connection.ExecuteScalarAsync<decimal>(sql);
                return averageInvoice;
            }
        }
    }
}
