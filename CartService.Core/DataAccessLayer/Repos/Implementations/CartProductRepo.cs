// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos.Implementations
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;
    using Dapper;
    using Microsoft.Extensions.Configuration;

    internal class CartProductRepo : BaseRepo, ICartProductRepo
    {
        public CartProductRepo(IConfiguration configuration)
            : base(configuration)
        {
        }

        public async Task<IEnumerable<CartProduct>> GetCartProductsAsync(int cartId)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "SELECT Id, CartId, ProductId, Amount FROM CartProducts WHERE CartId=@cartId";
                var cartProducts = await connection.QueryAsync<CartProduct>(sql, new { cartId });
                return cartProducts;
            }
        }

        public async Task<CartProduct> GetCartProductAsync(int cartId, int productId)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "SELECT Id, CartId, ProductId, Amount FROM CartProducts WHERE CartId=@cartId AND ProductId=@productId";
                var cartProduct = await connection.QueryFirstOrDefaultAsync<CartProduct>(sql, new { cartId, productId });
                return cartProduct;
            }
        }

        public async Task DeleteCartProductAsync(int cartProductId)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "DELETE FROM CartProducts WHERE Id=@cartProductId";
                await connection.ExecuteAsync(sql, new { cartProductId });
            }
        }

        public async Task AddCartProductAsync(int cartId, int productId, int amount)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "INSERT INTO CartProducts (CartId, ProductId, Amount) VALUES (@cartId, @productId, @amount)";
                await connection.ExecuteAsync(sql, new { cartId, productId, amount });
            }
        }

        public async Task UpdateCartProductAmountAsync(int cartProductId, int amount)
        {
            using (var connection = this.GetDbConnection())
            {
                var sql = "UPDATE CartProducts SET Amount = @amount WHERE Id = @cartProductId";
                await connection.ExecuteAsync(sql, new { amount, cartProductId });
            }
        }
    }
}
