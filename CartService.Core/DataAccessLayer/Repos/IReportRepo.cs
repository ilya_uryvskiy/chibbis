// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos
{
    using System.Threading.Tasks;

    internal interface IReportRepo
    {
        Task<int> GetCartCountAsync();

        Task<int> GetCartWithForBonusPointsProductCountAsync();

        Task<int> GetExpiredCartCountInDaysAsync(int days);

        Task<decimal> GetAverageInvoiceAsync();
    }
}
