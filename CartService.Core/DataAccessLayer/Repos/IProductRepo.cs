// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;

    internal interface IProductRepo
    {
        Task<IEnumerable<Product>> GetAllAsync();

        Task<Product> GetProductByIdAsync(int id);

        Task<Product> AddProductAsync(string name, decimal cost, bool forBonusPoints);
    }
}
