// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DataAccessLayer.Repos
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Chibbis.CartService.Core.DataAccessLayer.Models;

    internal interface ICartRepo
    {
        Task<bool> CheckCartExistsAsync(int id);

        Task<Cart> GetCartByIdAsync(int id);

        Task<Cart> AddCartAsync(DateTime lastAccessDt);

        Task SetLastAccessDtAsync(int id, DateTime lastAccessDt);

        Task<IEnumerable<Cart>> GetCartsWithLastAccessDtLessThenAsync(DateTime lastAccessDt);

        Task DeleteCartAsync(int id);
    }
}
