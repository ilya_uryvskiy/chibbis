// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.ReportModels
{
    public class DailyReport
    {
        public int CartCount { get; set; }

        public int CartWithProductForBonusPointsCount { get; set; }

        public int CartExpiresInTenDays { get; set; }

        public int CartExpiresInTwentyDays { get; set; }

        public int CartExpiresInThirtyDays { get; set; }

        public decimal AverageInvoice { get; set; }
    }
}
