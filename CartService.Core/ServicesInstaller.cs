// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core
{
    using Chibbis.CartService.Core.DataAccessLayer.Repos;
    using Chibbis.CartService.Core.DataAccessLayer.Repos.Implementations;
    using Chibbis.CartService.Core.DateTime;
    using Chibbis.CartService.Core.Services;
    using Chibbis.CartService.Core.Services.Implementations;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class ServicesInstaller
    {
        public static IServiceCollection AddCore(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            return serviceCollection
                .AddUtils()
                .AddRepos()
                .AddServices();
        }

        private static IServiceCollection AddUtils(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDateTime, CartDateTime>();
            serviceCollection.AddHttpClient();
            return serviceCollection;
        }

        private static IServiceCollection AddRepos(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IProductRepo, ProductRepo>();
            serviceCollection.AddTransient<ICartRepo, CartRepo>();
            serviceCollection.AddTransient<ICartProductRepo, CartProductRepo>();
            serviceCollection.AddTransient<ICartWebhookRepo, CartWebhookRepo>();
            serviceCollection.AddTransient<IReportRepo, ReportRepo>();
            return serviceCollection;
        }

        private static IServiceCollection AddServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IProductService, ProductService>();
            serviceCollection.AddTransient<ICartService, CartService>();
            return serviceCollection;
        }
    }
}
