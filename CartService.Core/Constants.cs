// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core
{
    public static class Constants
    {
        public const string DefaultConnectionStringName = "Sql";

        public const int CartExpireDays = 30;

        public const string CartWebhookAuthHeader = "X-CartAuth";
    }
}
