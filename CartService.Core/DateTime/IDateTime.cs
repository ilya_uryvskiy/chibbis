// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DateTime
{
    using System;

    public interface IDateTime
    {
        DateTime UtcNow { get; }
    }
}
