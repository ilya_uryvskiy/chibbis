// Copyright (c) Ilya Uryvskiy. All rights reserved.

namespace Chibbis.CartService.Core.DateTime
{
    using System;

    internal class CartDateTime : IDateTime
    {
        public DateTime UtcNow => DateTime.UtcNow;
    }
}
